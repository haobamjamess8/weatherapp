import 'package:flutter/material.dart';

const apiKey = "a32427e44c5c76e9d8d119334755b4d1";
const String openWeatherMapUrl =
    "https://api.openweathermap.org/data/2.5/weather";

const kTempTextStyle = TextStyle(
  fontFamily: 'Spartan MB',
  fontSize: 30.0,
);

const kMessageTextStyle = TextStyle(
  fontFamily: 'Spartan MB',
  fontSize: 30.0,
);

const kButtonTextStyle = TextStyle(
  fontSize: 30.0,
  fontFamily: 'Spartan MB',
);

const kConditionTextStyle = TextStyle(
  fontSize: 100.0,
);
