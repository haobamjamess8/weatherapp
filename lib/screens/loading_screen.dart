import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:weather_app/screens/location_screen.dart';
import 'package:weather_app/services/weather.dart';
import '../services/location.dart';
import '../services/networking.dart';
import 'package:http/http.dart' as http;
import '../utlilities/constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatefulWidget {
  static final String routeName = "/";
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
    _determinePosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Weather"),
      ),
      backgroundColor: Colors.black12,
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: const SpinKitChasingDots(
              color: Colors.white,
              size: 50.0,
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
          const Text(
            "Please wait....",
            style: TextStyle(color: Colors.white),
          )
        ],
      )),
    );
  }

  void _determinePosition() async {
    Map weatheDatas = await WeatherModel().getLocationWeather();

    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => LocationScreen(
        weatherData: weatheDatas,
      ),
    ));
  }
}
