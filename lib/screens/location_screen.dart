import 'package:flutter/material.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/screens/city_screen.dart';
import 'package:weather_app/services/weather.dart';
import 'package:weather_app/utlilities/constants.dart';

class LocationScreen extends StatefulWidget {
  static final String routeName = "/location-page";
  final Map weatherData;
  LocationScreen({required this.weatherData});

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  String temp = "";
  String msg = "";
  String icon = "";
  String location = "";

  @override
  void initState() {
    super.initState();
    updateUI(widget.weatherData);
  }

  void updateUiOnError(Map arg) {
    setState(() {
      temp = "0";
      msg = arg["msg"];
      icon = "";
      location = arg["city"];
    });
  }

  void updateUI(Map args) {
    WeatherDataModel weather = WeatherDataModel.fromJson(args);
    setState(() {
      if (args == null) {
        temp = "0";
        icon = "Error";
        location = "";
        msg = "Unable to get weather data";
        return;
      }
      temp = weather.temperature;
      msg = weather.message;
      icon = weather.weatherIcon;
      location = weather.location;

      print(temp);
    });
  }

  void navigate(BuildContext context) async {
    var cityName = await Navigator.of(context).pushNamed(CityScreen.routName);
    print("Location screen $cityName");
    if (cityName != null) {
      Map weatherData =
          await WeatherModel().getCityWeather(cityName.toString());
      print(weatherData);
      if (weatherData.containsKey("detail")) {
        print(weatherData["detail"]);

        updateUiOnError({
          "msg": weatherData["detail"],
          "city": cityName.toString(),
        });

        return;
      }
      updateUI(weatherData);
    }
  }

  @override
  Widget build(BuildContext context) {
    print(temp);

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/location_background.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.8), BlendMode.dstATop),
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: () async {
                      Map weatherData =
                          await WeatherModel().getLocationWeather();
                      updateUI(weatherData);
                    },
                    child: const Icon(
                      Icons.near_me,
                      size: 50.0,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      navigate(context);
                    },
                    child: const Icon(
                      Icons.location_city,
                      size: 50.0,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      " ${temp.toString()}ºC",
                      style: kTempTextStyle,
                    ),
                    Text(
                      icon,
                      style: kConditionTextStyle,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 11),
                child: Text(
                  "${msg} in $location",
                  textAlign: TextAlign.right,
                  style: kMessageTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class $ {}
