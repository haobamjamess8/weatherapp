import 'package:flutter/foundation.dart';

import '../services/weather.dart';

class WeatherDataModel {
  String temperature;
  String message;
  String weatherIcon;
  String location;

  WeatherDataModel({
    required this.temperature,
    required this.message,
    required this.weatherIcon,
    required this.location,
  });

  factory WeatherDataModel.fromJson(Map args) {
    WeatherModel weatherModel = WeatherModel();
    double temperature = args["main"]["temp"];
    int temp_ = temperature.toInt();
    String weatherIcon_ = weatherModel.getWeatherIcon(args["weather"][0]["id"]);
    String message_ = weatherModel.getMessage(temp_);
    String location_ = args["name"];

    return WeatherDataModel(
      temperature: temp_.toString(),
      message: message_,
      weatherIcon: weatherIcon_,
      location: location_,
    );
  }
}
