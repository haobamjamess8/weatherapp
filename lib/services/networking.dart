import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class ApiService {
  final String url;
  ApiService({required this.url});

  Future<dynamic> getData() async {
    Response response = await http.get(Uri.parse(this.url));
    Map<dynamic, dynamic> data = {};
    if (response.statusCode == 200) {
      data = jsonDecode(response.body);
    } else {
      data = {"detail": "Error on fetching"};
      log(response.statusCode.toString());
    }

    return data;
  }
}
