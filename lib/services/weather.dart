import '../services/networking.dart';

import '../services/location.dart';
import '../utlilities/constants.dart';

class WeatherModel {
  Future<Map> getCityWeather(String cityName) async {
    var url = "$openWeatherMapUrl?q=$cityName&units=metric&appid=$apiKey";
    ApiService service = ApiService(url: url);
    Map data = await service.getData();

    return data;
  }

  Future<Map> getLocationWeather() async {
    LocationService locationservice = LocationService();
    await locationservice.getCurrentLocation();

    String BASE_URL =
        "https://api.openweathermap.org/data/2.5/weather?lat=${locationservice.latitutde}&lon=${locationservice.longitude}&units=metric&appid=$apiKey";

    ApiService service = ApiService(url: BASE_URL);
    Map data = await service.getData();
    return data;
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
